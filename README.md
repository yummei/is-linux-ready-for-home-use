# Is linux ready for home use

No. For longer description: In many circumstances is usable, but can not daily use.  

This project is for linux home users to know about linux usability for normal users (Like Windows and MacOS users).
## A detailed description:

### Desktop

When use loptop or enable fractional scale, display may be not sharp:  
Desktop environment support fractional scale now, but software must update to support fractional scale widget toolkit.  
GTK introducing initial support for Wayland fractional scaling in GTK 4.11.1, enable use `GDK_DEBUG=gl-fractional`. [We’ve added support for the experimental wp_fractional_scale_manager_v1 protocol to the Wayland backend - gtk blog](https://blog.gtk.org/2023/04/05/gtk-4-11-1) [gnome:GNOME/gtk!5763](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/5763)  
QT support in QT6. [Support in Qt is only in Qt 6 right now - This week in KDE](https://pointieststick.com/2022/12/16/this-week-in-kde-wayland-fractional-scaling-oh-and-we-also-fixed-multi-screen)  

In libinput, mouse and touchpad accelerate not great:  
A like Windows and MacOS pr is not mergin. [freedesktop:libinput/libinput!426](https://gitlab.freedesktop.org/libinput/libinput/-/merge_requests/426) [freedesktop:libinput/libinput!350](https://gitlab.freedesktop.org/libinput/libinput/-/issues/350) [freedesktop:libinput/libinput!775 (merged)](https://gitlab.freedesktop.org/libinput/libinput/-/merge_requests/775)  

#### Gnome
Good UI and user experience, but performance not good:  
Many users report their Gnome has stuck. # need issues link  

Some Qt softwares no titlebar in Gnome with wayland. # need issues link  

#### KDE
Bad UI and user experience:  
Settings many items and not easy found what should users change.  
KDE and Qt software default icons not easy know what is this button to do.  

### Software
#### GTK
#TODO

#### Qt
Many Qt software has titlebar, menubar, menuicon(this not menubar), not easy use.  
#TODO

#### Firefox

Touchpad speed default is fast:  
The issues is fixed, but default value is not use. [bugzilla:firefox#1753033](https://bugzilla.mozilla.org/show_bug.cgi?id=1753033)  

#### Chrome

Chrome in wayland has some issues. #need detail  

#### backup

Many backup software but users do not easy found, and many software may not support flatpak or other install package's data backup.  
System backup not easy, not like Windows(may not work now) or MacOS.  

#### Media

#### IME and fonts

IME support not complete:  
Wayland support also dart. [freedesktop:wayland/wayland-protocols!112](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/112)  
Gnome only offical support iBus, many people use fcitx5.  
Some IME out-of-box experience not great, such as fcitx5.  

Many preinstall fonts may out-of-data, also in some wiki:  
Chinese fonts has Noto or Source fonts but some use wqy fonts.  

### Kernel (System)

Low ram maybe make desktop freeze:  
For now has some oomd software, but may not enable.  

When use systemd-oomd, it has kill all cgroup threads, so the browser closes fully, and not like Windows close one tab.  

May can not hibernate when swap space is full:  
This issues is hibernate use swap space.  
